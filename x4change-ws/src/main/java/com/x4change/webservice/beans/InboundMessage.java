package com.x4change.webservice.beans;

import org.apache.commons.lang3.StringUtils;

public class InboundMessage {

	/**
	 * A 34 character unique identifier for the message. May be used to later
	 * retrieve this message from the REST API.
	 */
	private String messageSid;
	/**
	 * Same value as MessageSid. Deprecated and included for backward
	 * compatibility.
	 */
	private String smsSid;
	/**
	 * The 34 character id of the Account this message is associated with.
	 */
	private String accountSid;
	/**
	 * The 34 character id of the Messaging Service associated to the message.
	 */
	private String messagingServiceSid;
	/**
	 * The phone number that sent this message.
	 */
	private String from;
	/**
	 * The phone number of the recipient.
	 */
	private String to;
	/**
	 * The text body of the message. Up to 1600 characters long.
	 */
	private String body;
	/**
	 * The city of the sender
	 */
	private String fromCity;
	/**
	 * The state or province of the sender.
	 */
	private String fromState;
	/**
	 * The postal code of the called sender.
	 */
	private String fromZip;
	/**
	 * The country of the called sender.
	 */
	private String fromCountry;
	/**
	 * The city of the recipient.
	 */
	private String toCity;
	/**
	 * The state or province of the recipient.
	 */
	private String toState;
	/**
	 * The postal code of the recipient.
	 */
	private String toZip;
	/**
	 * The country of the recipient.
	 */
	private String toCountry;

	private InboundMessage(Builder builder) {
		messageSid = builder.messageSid;
		smsSid = builder.smsSid;
		accountSid = builder.accountSid;
		messagingServiceSid = builder.messagingServiceSid;
		from = builder.from;
		to = builder.to;
		body = builder.body;

		fromCity = builder.fromCity;
		fromState = builder.fromState;
		fromZip = builder.fromZip;
		fromCountry = builder.fromCountry;
		toCity = builder.toCity;
		toState = builder.toState;
		toZip = builder.toZip;
		toCountry = builder.toCountry;
	}

	public static class Builder {

		private String messageSid;
		private String smsSid;
		private String accountSid;
		private String messagingServiceSid;
		private String from;
		private String to;
		private String body;

		public Builder(String messageSid, String smsSid, String accountSid,
				String messagingServiceSid, String from, String to, String body) {

			super();
			this.messageSid = messageSid;
			this.smsSid = smsSid;
			this.accountSid = accountSid;
			this.messagingServiceSid = messagingServiceSid;
			this.from = from;
			this.to = to;
			this.body = body;
		}

		private String fromCity = StringUtils.EMPTY;
		private String fromState = StringUtils.EMPTY;
		private String fromZip = StringUtils.EMPTY;
		private String fromCountry = StringUtils.EMPTY;
		private String toCity = StringUtils.EMPTY;
		private String toState = StringUtils.EMPTY;
		private String toZip = StringUtils.EMPTY;
		private String toCountry = StringUtils.EMPTY;

		public Builder fromCity(String val) {
			fromCity = val;
			return this;
		}

		public Builder fromState(String val) {
			fromState = val;
			return this;
		}

		public Builder fromZip(String val) {
			fromZip = val;
			return this;
		}

		public Builder fromCountry(String val) {
			fromCountry = val;
			return this;
		}

		public Builder toCity(String val) {
			toCity = val;
			return this;
		}

		public Builder toState(String val) {
			toState = val;
			return this;
		}

		public Builder toZip(String val) {
			toZip = val;
			return this;
		}
		
		public Builder toCountry(String val) {
			toCountry = val;
			return this;
		}

		public InboundMessage build() {
			return new InboundMessage(this);
		}

	}

	public String getMessageSid() {
		return messageSid;
	}

	public String getSmsSid() {
		return smsSid;
	}

	public String getAccountSid() {
		return accountSid;
	}

	public String getMessagingServiceSid() {
		return messagingServiceSid;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public String getBody() {
		return body;
	}

	public String getFromCity() {
		return fromCity;
	}

	public String getFromState() {
		return fromState;
	}

	public String getFromZip() {
		return fromZip;
	}

	public String getFromCountry() {
		return fromCountry;
	}

	public String getToCity() {
		return toCity;
	}

	public String getToState() {
		return toState;
	}

	public String getToZip() {
		return toZip;
	}

	public String getToCountry() {
		return toCountry;
	}



}
