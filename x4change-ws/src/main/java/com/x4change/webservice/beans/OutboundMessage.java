package com.x4change.webservice.beans;


public class OutboundMessage {

	private String from;
	/**
	 * The phone number of the recipient.
	 */
	private String to;
	/**
	 * The text body of the message. Up to 1600 characters long.
	 */
	private String body;

	private OutboundMessage(Builder builder) {
		to = builder.to;
		body = builder.body;
	}

	public static class Builder {

		private String to;
		private String body;

		public Builder(String to, String body) {

			super();
			this.to = to;
			this.body = body;
		}
		
		public OutboundMessage build() {
			return new OutboundMessage(this);
		}
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public String getBody() {
		return body;
	}
}
