package com.x4change.webservice.client;

import org.springframework.stereotype.Component;

@Component
public class SmsBoxClient {
	
	/*
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsBoxClient.class);

    @Value("${smsbox.rootUrl:api.smsbox.fr/api.php}")
    protected String rootUrl;

    @Value("${smsbox.timeout:30000}")
    private int timeout;

    @Value("${smsbox.apiKey:pub-a5c55ada7149307caff3934c58a0ca4e}")
    private String apiKey;


    public SmsResponse send(Sms sms) {
        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = prepareHttpRequest(sms);

        try {
            HttpResponse response = httpClient.execute(httpGet);
            int httpcode = response.getStatusLine().getStatusCode();
            return new SmsResponse(httpcode, null);
        } catch (IOException ex) {
            LOGGER.debug("Echec de l'appel à SmsBox");
            return null;
        }
    }

    private HttpGet prepareHttpRequest(Sms sms) {
        try {
            URIBuilder builder = new URIBuilder();
            builder.setScheme("http").setHost(rootUrl).setPath("/")
                    .setParameter("msg", sms.getContent())
                    .setParameter("dest", sms.getRecipient())
                    .setParameter("apiKey", apiKey)
                    .setParameter("mode", "Standard")
                    .setParameter("id", "1");
            URI uri = builder.build();

            HttpGet httpGet = new HttpGet(uri);

            return httpGet;
        } catch (URISyntaxException e) {
            LOGGER.debug("Echec de la création de la requete Http pour SmsBox");
            e.printStackTrace();
            return null;
        }
    }
*/
//    public static void main(String[] args) {
//        Sms sms = new Sms("0033629135328", "Agla kaka yi so");
//        SmsRequest smsRequest = new SmsRequest(sms);
//        SmsResponse smsResponse = new SmsBoxClient().send(smsRequest);
//        System.out.println("Reponse de SmsBox" + smsResponse.hashCode());
//    }
    
    
    class Sms {
    	private String content;
    	private String recipient;
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getRecipient() {
			return recipient;
		}
		public void setRecipient(String recipient) {
			this.recipient = recipient;
		}
    	
    }
    
    class SmsResponse {
    	
    	public SmsResponse(final int toto, final String tata) {
    		
    	}
    }
}
