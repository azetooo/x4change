package com.x4change.webservice.controller;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.twilio.sdk.verbs.TwiMLResponse;
import com.x4change.webservice.beans.InboundMessage;
import com.x4change.webservice.beans.OutboundMessage;
import com.x4change.webservice.service.MessageProcessingService;

@RestController
public class SmsController {

	@Autowired
	@Qualifier("msgProcessingService")
	private MessageProcessingService msgProcessingService;

	@RequestMapping(value = "/testSK", method = { GET }, produces = "application/json;charset=UTF-8")
	ResponseEntity<DateTime> test() {
		DateTime response = DateTime.now();
		return new ResponseEntity<DateTime>(response, OK);
	}

	@RequestMapping(value = "/sendSms", method = { GET }, produces = "application/json;charset=UTF-8")
	public void sendTo(@RequestParam(value = "to") String to,
			@RequestParam(value = "body") String body) {
		final OutboundMessage goingSms = new OutboundMessage.Builder(to, body)
				.build();

		msgProcessingService.sendDiret(goingSms);
	}
	
	@RequestMapping(value = "/postSms", method = { POST }, produces = "application/json;charset=UTF-8")
	public void postSMS(@RequestParam(value = "to") String to,
			@RequestParam(value = "body") String body) {
		final OutboundMessage goingSms = new OutboundMessage.Builder(to, body)
				.build();

		msgProcessingService.sendDiret(goingSms);
	}

	@RequestMapping(value = "/sms", method = { POST }, produces = "application/xml;charset=UTF-8")
	public @ResponseBody String computeIncomingSms(
			@RequestParam(value = "MessageSid") String messageSid,
			@RequestParam(value = "SmsSid") String smsSid,
			@RequestParam(value = "AccountSid") String accountSid,
			@RequestParam(value = "SmsMessageSid") String messagingServiceSid,
			@RequestParam(value = "From") String from,
			@RequestParam(value = "To") String to,
			@RequestParam(value = "Body") String body,
			@RequestParam(value = "FromCity", required = false) String fromCity,
			@RequestParam(value = "FromState", required = false) String fromState,
			@RequestParam(value = "FromZip", required = false) String fromZip,
			@RequestParam(value = "FromCountry", required = false) String fromCountry,
			@RequestParam(value = "ToCity", required = false) String toCity,
			@RequestParam(value = "ToState", required = false) String toState,
			@RequestParam(value = "ToZip", required = false) String toZip,
			@RequestParam(value = "ToCountry", required = false) String toCountry) {

		final InboundMessage incomingSms = new InboundMessage.Builder(
				messageSid, smsSid, accountSid, messagingServiceSid, from, to,
				body).fromCity(fromCity).fromState(fromState).fromZip(fromZip)
				.fromCountry(fromCountry).toCity(toCity).toState(toState)
				.toZip(toZip).toCountry(toCountry).build();

		final TwiMLResponse response = msgProcessingService
				.respondeAuto(incomingSms);
		System.out.println(response.toXML());
		return response.toEscapedXML();
	}
}
