package com.x4change.webservice.service;

import com.twilio.sdk.verbs.TwiMLResponse;
import com.x4change.webservice.beans.InboundMessage;
import com.x4change.webservice.beans.OutboundMessage;

public interface MessageProcessingService {
	
	public void process(final InboundMessage inboundMsg) ; 
	
	public TwiMLResponse respondeAuto(final InboundMessage inboundMsg) ;
	
	
	public void sendDiret(final OutboundMessage outboundMsg) ;

}
