package com.x4change.webservice.service;

import com.x4change.webservice.beans.InboundMessage;
import com.x4change.webservice.beans.OutboundMessage;

public interface SmsTwilioService {
	
	public void receiveSms (final InboundMessage inboundMsg) ;
	
	public void sendSms (final OutboundMessage outboundMsg);

}
