package com.x4change.webservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;
import com.x4change.webservice.beans.InboundMessage;
import com.x4change.webservice.beans.OutboundMessage;
import com.x4change.webservice.service.MessageProcessingService;
import com.x4change.webservice.service.SmsTwilioService;

@Component("msgProcessingService")
public class MessageProcessingServiceImpl implements MessageProcessingService {

	@Autowired
	@Qualifier("smsTwilioService")
	private SmsTwilioService smsTwilioService;
	
	public void process(InboundMessage inboundMsg) {
		// TODO Auto-generated method stub
		
	}

	public TwiMLResponse respondeAuto(InboundMessage inboundMsg) {
		// sauvegarder le message
		
		// retourner une réponse
		final TwiMLResponse response = new TwiMLResponse();
		final Message message = new Message("Test succes");
		message.setTo(inboundMsg.getFrom());
		message.setFrom(inboundMsg.getTo());
		try {
			response.append(message);
		} catch (TwiMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	public void sendDiret(OutboundMessage outboundMsg) {
		smsTwilioService.sendSms(outboundMsg);
	}

}
