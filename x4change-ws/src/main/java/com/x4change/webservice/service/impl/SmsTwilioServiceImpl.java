package com.x4change.webservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.x4change.webservice.beans.InboundMessage;
import com.x4change.webservice.beans.OutboundMessage;
import com.x4change.webservice.service.SmsTwilioService;

@Component("smsTwilioService")
public class SmsTwilioServiceImpl implements SmsTwilioService {
	
	@Autowired
    Environment env;
	
	@Value("${twilio.account.id}")
	private String accountId;
	
	@Value("${twilio.auth.token}")
	private String authToken;
	
	@Value("${twilio.phone.number}")
	private String phoneNumber;
	

    private TwilioRestClient client;


	public SmsTwilioServiceImpl() {
		accountId = "AC0ddace74fa2b3cb4bc9392593dd6d26d";
		authToken = "2688b346723d29b9ae327ccd57790715";
		phoneNumber = "+15146136007";
		Assert.notNull(accountId);
		Assert.notNull(authToken);
		
		this.client = new TwilioRestClient(accountId, authToken);
	}


	public void receiveSms(final InboundMessage inboundMsg) {
		// TODO Auto-generated method stub
		
	}


	public void sendSms(final OutboundMessage outboundMsg) {
		
		final List<NameValuePair> params = getOutboundMessageParams(outboundMsg);
		
		try {
            this.client.getAccount().getMessageFactory().create(params);
        } catch (TwilioRestException exception) {
            exception.printStackTrace();
        }
	}
    
    
	private List<NameValuePair> getOutboundMessageParams(final OutboundMessage outboundMsg) {
        
		Assert.notNull(outboundMsg);
		
		final String body = outboundMsg.getBody();
		Assert.notNull(body);
		
		final String to = outboundMsg.getTo();
		Assert.notNull(to);
		
		final List<NameValuePair> params = new ArrayList<NameValuePair>();
        
		params.add(new BasicNameValuePair("Body", body));
        params.add(new BasicNameValuePair("To", to));
        params.add(new BasicNameValuePair("From", phoneNumber));;

        return params;
    }
    
	

}
